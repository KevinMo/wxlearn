﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.Entities.Request;
using WxLearn.MessageHandler;
using Senparc.Weixin.MP.MvcExtension;

namespace WxLearn.Controllers
{
    public class WeixinController : Controller
    {
        public readonly string Token = "moshuchao";
        public static readonly string EncodingAESKey = "PuvUhSzj8LiFLPk2ZHg5wgdRLqGSWQsTQwxv8Um6dy0";//与微信公众账号后台的EncodingAESKey设置保持一致，区分大小写。
        public static readonly string AppId = "wxb256de154b4ccd4f";//与微信公众账号后台的AppId设置保持一致，区分大小写。



        [HttpGet]
        // GET: Weixin
        public ActionResult Index(string signature,string timestamp,string nonce,string echostr)
        {
            if (CheckSignature.Check(signature,timestamp,nonce,Token))
            {
                return Content(echostr);
            }
            else
            {
                return Content("failed:" + signature + "," + CheckSignature.GetSignature(timestamp, nonce, Token) + "。如果您在浏览器中看到这条信息，表明此Url可以填入微信后台。");
            }
            
        }

        public ActionResult Index(PostModel pPostModel)
        {
            if (!CheckSignature.Check(pPostModel.Signature, pPostModel.Timestamp, pPostModel.Nonce, Token))
            {
                return Content("参数错误");
            }
            pPostModel.Token = Token;
            pPostModel.EncodingAESKey = EncodingAESKey;
            pPostModel.AppId = AppId;
            var messageHandler = new CustomMessageHandler(Request.InputStream, pPostModel);
            messageHandler.Execute();
            return new FixWeixinBugWeixinResult(messageHandler);

        }

        
    }

    
}