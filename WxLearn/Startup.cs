﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WxLearn.Startup))]
namespace WxLearn
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
